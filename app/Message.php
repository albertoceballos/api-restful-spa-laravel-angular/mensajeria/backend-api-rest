<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table='messages';
    
    //relación many to one con user
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    
    //relación many to one
    public function chat(){
        return $this->belongsTo('App\Chat','chat_id');
    }
    
}
