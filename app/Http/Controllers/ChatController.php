<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//modelos
use App\Chat;
use App\Message;
//servicios
use App\Helpers\JwtAuth;

class ChatController extends Controller {

    //extraer un chat por su id
    public function getChat($chatId, Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            $mensajes = Message::where('chat_id', $chatId)->get()->load('user');
            $chat= Chat::where('id',$chatId)->get()->load('user1')->load('user2');
            //var_dump($mensajes);
            //die()
            if (count($mensajes) > 0) {
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'messages' => $mensajes,
                    'chat'=>$chat
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'success',
                    'message' => 'No hay mensajes que mostrar',
                    'chat'=>$chat
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }

        return response()->json($data);
    }
    
    //extrer chat por sus usuarios
    public function getChatByUser($contact_id,Request $request, JwtAuth $jwtAuth){
         $token=$request->header('Authorization');
         $auth=$jwtAuth->checkToken($token);
         if($auth){
             $contact_id=(int)$contact_id;
             $identity=$jwtAuth->checkToken($token,true);
             $user_id=$identity->sub;
             $chat= Chat::whereIn('user1_id',[$contact_id,$user_id])->whereIn('user2_id',[$contact_id,$user_id])->first();
             if(!empty($chat)){
                 $chatId=$chat->id;
             }else{
                 //si no existe el chat entre los usuarios se creará uno nuevo
                 $chat_obj=new Chat();
                 $chat_obj->user1_id=$user_id;
                 $chat_obj->user2_id=$contact_id;
                 $chat_obj->created_at=new \DateTime('now');
                 $chat_obj->save();
                 $chatId=$chat_obj->id;
             }
             return $chatId;
         }else{
             $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
         }
    }

    //extraer todos los chats del usuario logueado
    public function getUserChats(Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //Buscar chats deonde participa el usuario
            $chats = Chat::where('user1_id', $user_id)->orWhere('user2_id', $user_id)->get()->load('messages')->load('user1')->load('user2');
            if (count($chats) > 0) {
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'chats' => $chats,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'No hay ningún chat del usuario',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida',
            ];
        }
        return response()->json($data);
    }

}
