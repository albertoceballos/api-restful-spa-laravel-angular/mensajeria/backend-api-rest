<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//modelos
use App\Following;
use App\User;
//servicios
use App\Helpers\JwtAuth;

class FollowingController extends Controller {

    public function allFollows() {

        $follows = Following::get()->load('followed');

        if (!empty($follows)) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'follows' => $follows,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No hay follows',
            ];
        }
        return response()->json($data);
    }

    //Guardar nuevo follow(añadir contacto)
    public function addFollow(Request $request, JwtAuth $jwtAuth, $followed_id) {
        //comprobar token 
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //extraer usuario logueado del token
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //conseguir usuario al que quiero seguir
            //$json = $request->input('json');
            //$params = json_decode($json);
            //$followed_id = (int) $params->followed;
            //comprobar que existe el usuario al que seguir
            $exists_user = User::find($followed_id);
            if (!empty($exists_user)) {
                //crear nuevo objeto following
                $following = new Following();
                $following->user_id = $user_id;
                $following->followed = $followed_id;
                //var_dump($following);
                //die();
                //guardar
                $following->save();
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'follow' => $following,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'No existe el usuario al que quieres seguir',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }
        return response()->json($data);
    }

    //borrar follow
    public function deleteFollow(Request $request, JwtAuth $jwtAuth, $followed_id) {
        //conseguir y comprobar token
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //identidad de usuario logueado
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;

            //Buscar el follow en el que el usuario logueado es user_id y el followed es el que quiere dejar de seguir
            $follow = Following::where(['user_id' => $user_id, 'followed' => $followed_id])->first();
            if (!empty($follow)) {
                //borrar follow
                $follow->delete();
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'message' => 'Has dejado de seguir al usuario',
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'No existe el follow',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }
        return response()->json($data);
    }

    //mostrar todos los usuarios a los que sigue el usuario logueado (contactos);
    public function getContacts(Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        //si el token es válido
        if ($auth) {
            $identity=$jwtAuth->checkToken($token,true);
            $user_id=$identity->sub;
            //buscar los usuarios a los que sigues
            $followed= Following::where('user_id',$user_id)->get();
            $users=Following::where('user_id',$user_id)->get()->load('followed');
            if(count($followed)>0){
                $data=[
                    'code'=>200,
                    'status'=>'success',
                    'follows'=>$followed,
                    'users'=>$users,
                ];
            }else{
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'No sigues a ningún usuario',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }
        return response()->json($data);
    }

}
