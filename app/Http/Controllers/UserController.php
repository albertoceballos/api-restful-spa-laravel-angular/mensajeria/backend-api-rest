<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
//modelo de usuario
use App\User;
//servicio
use App\Helpers\JwtAuth;

class UserController extends Controller {

    public $messages;

    public function __construct() {
        $this->messages = [
            'required' => 'El :attribute es un campo requerido.',
            'min' => 'El :attribute debe tener al menos :min caracteres',
        ];
    }

    public function register(Request $request) {
        //recoger datos
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);
        if (!empty($json)) {
            //validar datos
            $validator = Validator::make($params_array, [
                        'name' => 'required',
                        'nick' => 'required|max:20',
                        'email' => 'required|email',
                        'password' => 'required|min:5',
                            ], $this->messages);
            if ($validator->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Los datos no son válidos',
                    'errors' => $validator->errors(),
                ];
            } else {
                //comprobar si ya existe el e-mail
                $isset_email = User::where('email', $params->email)->get();
                if (count($isset_email) > 0) {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'El e-mail ya existe, prueba con otro',
                    ];
                } else {
                    //creamos nuevo objeto de usuario y seteamos propiedades
                    $user = new User();
                    $user->role = 'ROLE_USER';
                    $user->name = $params->name;
                    $user->surname = !empty($params->surname) ? $params->surname : null;
                    $user->nick = $params->nick;
                    $user->email = $params->email;
                    //cifrar password
                    $pwd = hash('sha256', $params->password);
                    $user->password = $pwd;
                    $user->created_at = new \DateTime('now');

                    //guardar usuario
                    $user->save();
                    //datos con éxito
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Usuario registrado con éxito',
                        'user' => $user,
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Faltan datos',
            ];
        }
        return response()->json($data);
    }

    public function login(Request $request, JwtAuth $jwtAuth) {
        $json = $request->input('json');
        $params = json_decode($json);
        $params_array = json_decode($json, true);
        if (!empty($json)) {
            //validar datos
            $validator = Validator::make($params_array, [
                        'email' => 'required|email',
                        'password' => 'required|min:5'
                            ], $this->messages);
            if ($validator->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Los datos no son válidos',
                    'message' => $validator->errors(),
                ];
            } else {
                //si llegan los datos necesarios
                $email = $params->email;
                $password = hash('sha256', $params->password);
                $getToken = !empty($params->getToken) ? $params->getToken : null;

                //llamo al servicio jwtAuth y si llega getToken a true devuelve el token y si no la identidad
                if ($getToken) {
                    $signUp = $jwtAuth->signUp($email, $password, true);
                } else {
                    $signUp = $jwtAuth->signUp($email, $password);
                }

                $data = $signUp;
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Faltan datos',
            ];
        }
        return response()->json($data);
    }

    //editar usuario
    public function update(Request $request, JwtAuth $jwtAuth) {
        //consigo el token de la cabecera
        $token = $request->header('Authorization');
        //compruebo si la autenticación es buena con el servicio jwtAuth
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //sacar la identidad del usuario
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //conseguir datos de usuario a actualizar
            $json = $request->input('json');
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            $validator = Validator::make($params_array, [
                        'name' => 'required',
                        'nick' => 'required|max:20',
                        'email' => 'required|email',
                            ], $this->messages);
            if ($validator->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Los datos no son válidos',
                    'errors' => $validator->errors(),
                ];
            } else {
                //sacar objeto de usuario a actualizar
                $user = User::find($user_id);

                //comprobar que el email no está repetido
                $isset_email = User::where('email', $params->email)->first();
                //actualizar solo si no encuentra el email o si es el mismo que ya tenía 
                if (empty($isset_email) || $isset_email->email == $identity->email) {
                    //setear datos
                    $user->name = $params->name;
                    $user->surname = !empty($params->surname) ? $params->surname : null;
                    $user->nick = $params->nick;
                    $user->email = $params->email;
                    $user->updated_at = new \DateTime('now');
                    //guardar
                    $user->save();
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Usuario actualizado con éxito',
                        'user' => $user,
                    ];
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Error, el email ya existe',
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida',
            ];
        }
        return response()->json($data);
    }

    //subir avatar
    public function uploadAvatar(Request $request, JwtAuth $jwtAuth) {
        //conseguir token de la cabecera y comprobar
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //conseguiir identidad de usuario mediante el token
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //conseguir objeto de usuario
            $user = User::find($user_id);

            //conseguir archivo del avatar
            $file = $request->file('file0');
            if (!empty($file)) {
                //comprobar extension de archivo
                $extension = $file->getClientOriginalExtension();
                if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png') {
                    //poner nombre al archivo
                    $file_name = $file->getClientOriginalName();
                    $file_path = $user_id . time() . $file_name;
                    //guardar archivo en disco
                    $file->storeAs('users/avatars', $file_path);
                    //setear nombre del archivo al objeto user y guardar objeto
                    $user->image = $file_path;
                    $user->save();
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Avatar subido con éxito',
                        'user' => $user,
                    ];
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'La extensión del archivo no es válida',
                    ];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autenticación inválida',
            ];
        }
        return response()->json($data);
    }

    public function getAvatar($filename) {
        $file = Storage::disk('avatars')->get($filename);

        return new Response($file);
    }

    //listar todos los usuarios de la BBDD;
    public function listAll() {
        $users = User::get();

        if (count($users) > 0) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'users' => $users,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No hay usuarios',
            ];
        }

        return response()->json($data);
    }

    //buscar contacto
    public function searchUser($searchString = null) {
        //búsqueda en caso de que venga una cadena
        if (!empty($searchString)) {
            $users = User::where('name', 'LIKE', '%' . $searchString . '%')
                    ->orWhere('surname', 'LIKE', '%' . $searchString . '%')
                    ->orWhere('nick', 'LIKE', '%' . $searchString . '%')
                    ->get();
        } else {
            //si viene vacía la cadena de búsqueda que muestre todos los usuarios
            $users = User::get();
        }

        if (count($users) > 0) {
            $data = [
                'code' => 200,
                'status' => 'success',
                'users' => $users,
            ];
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'No hay coincidencias',
            ];
        }


        return response()->json($data);
    }

}
