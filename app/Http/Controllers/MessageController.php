<?php

namespace App\Http\Controllers;

//Modelos
use App\Message;
use App\Chat;
//servicios
use App\Helpers\JwtAuth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class MessageController extends Controller {

    public function index() {

        $mensajes = Message::get()->load('user', 'chat');

        $data = [
            'code' => 200,
            'status' => 'success',
            'messages' => $mensajes,
        ];
        return response()->json($data);
    }

    //mandar nuevo mensaje
    public function send(Request $request, JwtAuth $jwtAuth) {
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //identidad del usuario logueado
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //extraer mensaje enviado
            $json = $request->input('json');
            $params = json_decode($json);
            $message = !empty($params->message) ? $params->message : null;
            $chat_id = !empty($params->chat_id) ? $params->chat_id: null;
            $image = !empty($params->image) ? $params->image : null;
            if (!empty($message) && !empty($chat_id)) {
                $isset_chat = Chat::find($chat_id);
                if (!empty($isset_chat)) {
                    //crear nuevo objeto de mensaje
                    $message_obj = new Message();
                    $message_obj->user_id = $user_id;
                    $message_obj->chat_id = $chat_id;
                    $message_obj->message = $message;
                    $message_obj->image = $image;
                    $message_obj->readed = 0;
                    $message_obj->created_at = new \DateTime('now');
                    //guardar nuevo mensaje
                    $message_obj->save();

                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Nuevo mensaje mandado con éxito',
                    ];
                }else{
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'No existe el chat',
                    ];
                }
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Faltan datos',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }
        return response()->json($data);
    }

    //subir imagen de publicación
    public function uploadImage(Request $request) {
        $file = $request->file('file0');
        if (!empty($file)) {
            $extension = $file->getClientOriginalExtension();
            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png') {
                $file_name = md5(uniqid()) . "." . $extension;
                $file->storeAs('messages/images', $file_name);
                $data = [
                    'code' => 200,
                    'status' => 'success',
                    'file' => $file_name,
                ];
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'La extensión no es válida',
                ];
            }
        }
        return response()->json($data);
    }

    //borrar mensaje
    public function delete(Request $request, JwtAuth $jwtAuth, $message_id) {
        $token = $request->header('Authorization');
        $auth = $jwtAuth->checkToken($token);
        if ($auth) {
            //extraer identidad de usuario del token
            $identity = $jwtAuth->checkToken($token, true);
            $user_id = $identity->sub;
            //obtener objeto de mensaje por el id del mensaje que llega
            $message = Message::find($message_id);
            //comprobar si existe el mensaje
            if (!empty($message)) {
                //comprobar que el mensaje es propiedad del usuario
                if ($message->user_id == $user_id) {
                    //borrar mensaje
                    $message->delete();
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'message' => 'Mensaje borrado con éxito',
                    ];
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'message' => 'Sin autorización para borrar el mensaje',
                    ];
                }
            } else {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'El mensaje no existe',
                ];
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'message' => 'Autorización inválida',
            ];
        }
        return response()->json($data);
    }

}
