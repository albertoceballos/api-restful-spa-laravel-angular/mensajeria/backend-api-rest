<?php

namespace App\Helpers;

use Firebase\JWT\JWT;

//modelo user
use App\User;

class JwtAuth{
    
    private $key;
    
    public function __construct() {
        $this->key="Una_clave_ultrasecreta";
    }
    //genera tokens al hacer login en la App
    public function signUp($email,$password,$getToken=null){
        $user= User::where(['email'=>$email,'password'=>$password])->first();
        if(!empty($user)){
            $signup=true;
        }else{
            $signup=false;
        }
        
        if($signup){
            
            $token=[
              'sub'=>$user->id,
              'name'=>$user->name,
              'surname'=>$user->surname,
              'email'=>$user->email,
              'nick'=>$user->nick,
              'image'=>$user->image,
              'iat'=>time(),
              'exp'=> time()+(60*60*24*7),  
            ];
            
            //token codificado
                $jwt= JWT::encode($token, $this->key, 'HS256');
            //token descodificado
                $decoded= JWT::decode($jwt, $this->key, ['HS256']);
                
            //devuelvo el token codificado si viene getToken a true, sino devuelve la identida (el token decodificado)    
            if($getToken){
                return $jwt;
            }else{
                return $decoded;
            }
        }else{
            return ['status'=>'error','message'=>'El login ha fallado'];
        }
        
    }
    
    //comprobar tokens 
    public function checkToken($token,$getIdentity=null){
        
        $auth=false;
        
        //decodificamos el token
        try {
            $decoded= JWT::decode($token, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $ex) {
            $auth=false;
            
        } catch (\DomainException $ex){
            $auth=false;
        }
        
        //si es correcto auth será true si no seaá false
        if(!empty($decoded) && $decoded->sub){
            $auth=true;
        }else{
            $auth=false;
        }
        
        //si no s llega getIdentity a true que la función devuelva  la identidad si no devuelve auth ya sea true o false
        if($getIdentity){
            return $decoded;
        }
        return $auth;
    }
    
    
}
