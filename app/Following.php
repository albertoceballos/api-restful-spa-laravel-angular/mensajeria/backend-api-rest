<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Following extends Model
{
    protected $table='following';
    
    public function followed(){
        return $this->belongsTo('App\User','followed');
    }
}
