<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table='chats';
    
    public function messages(){
        return $this->hasMany('App\Message');
    }
    
    public function user1(){
        return $this->hasOne('App\User', 'id', 'user1_id');
    }
    
    public function user2(){
        return $this->hasOne('App\User', 'id', 'user2_id');
    }
    
}
