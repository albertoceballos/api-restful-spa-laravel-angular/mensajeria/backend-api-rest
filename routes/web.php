<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});



Route::prefix('api')->group(function () {

//rutas de UserController
    
    //registro de usuario
    Route::post('/register','UserController@register');
    //login de usuario
    Route::post('/login','UserController@login');
    //listar todos los usuarios
    Route::get('all-users','UserController@listAll');
    //actualizar usuarios
    Route::post('user/update','UserController@update');
    //subir avatar
    Route::post('user/upload-avatar','UserController@uploadAvatar');
    //mostrar avatar
    Route::get('user/get-avatar/{filename}','UserController@getAvatar');
    //Buscar usuarios
    Route::get('user/search/{searchString?}', 'UserController@searchUser');
    
            
//followingController
    Route::get('/follow/all','FollowingController@allFollows');
    //añadir contacto
    Route::get('/follow/add/{followed_id}','FollowingController@addFollow');
    //borrar contacto
    Route::get('/follow/delete/{followed_id}','FollowingController@deleteFollow');
    //Mostrar usaurios a los que sigue el usuario logueado (contactos)
    Route::get('user/contacts','FollowingController@getContacts');
    
//messageController
    Route::get('/messages', 'MessageController@index');
    //mandar un nuevo mensaje
    Route::post('message/send','MessageController@send');
    //borrar mensaje
    Route::get('message/delete/{message_id}','MessageController@delete');
    //subir imagen de publicación
    Route::post('message/upload-image','MessageController@uploadImage');
    
//ChatController
    
    //extraer un chat por su id
    Route::get('/chat/{chatId}', 'ChatController@getChat');
    //extraer todos los chats del usuario logueado
    Route::get('/chats','ChatController@getUserChats');
    
    //extaer chat por el contacto del usuario
    Route::get('/chatByUser/{contact_id}','ChatController@getChatByUser');
});





